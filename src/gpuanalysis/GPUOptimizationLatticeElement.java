/*
 * Author: Jonathan Mansur
 * 
 * This code is meant to analyze for loops to determine if they can be parallelized for
 * the Aparapi Framework.
 * 
 */
package gpuanalysis;

import java.util.Map;
import java.util.TreeMap;

import edu.cmu.cs.crystal.util.Copyable;

public class GPUOptimizationLatticeElement implements Copyable<GPUOptimizationLatticeElement> {

	private String index;
	private GPUOptimizationStates state; 
	private long score;
	
	//Used to aid in copying arrays
	private GPUOptimizationLatticeElement(GPUOptimizationStates state,String index) {
		this.index = index;
		this.state = state;
		this.score = 0;
	}
	
	private GPUOptimizationLatticeElement(GPUOptimizationStates state,String index,long score) {
		this.index = index;
		this.state = state;
		this.score = score;
	}
	
	@Override
	public GPUOptimizationLatticeElement copy() {
		return new GPUOptimizationLatticeElement(state, index, score);
	}

	//Helper function handles the write transfer function
	public GPUOptimizationLatticeElement write(String index) {
		boolean indexMatch = indexMatch(this.index,index);
		
		if(isInvalidType()) {
			return this.use();
		}
		
		if(this.state == GPUOptimizationStates.INVALID) {
			return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null,score);
		}
		
		if (this.state == GPUOptimizationStates.ASSIGNED) {
			//If the array has not been read from then set the array to read from the given index
			return new GPUOptimizationLatticeElement(GPUOptimizationStates.WRITE_INDEX,index,score);
		}
		
		if(indexMatch) {
			if ( (this.state == GPUOptimizationStates.READ_WRITE_INDEX) ||
				 (this.state == GPUOptimizationStates.WRITE_INDEX) ) {
				//If indexes match and they are all writes keep the current state
				return this.copy();
			} else if(this.state == GPUOptimizationStates.READ_INDEX) {
				//Go to read write if the array has previously been read from and the indexes match
				return new GPUOptimizationLatticeElement(GPUOptimizationStates.READ_WRITE_INDEX,index,score);
			} else {
				//This is the read-multi case 
				return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null);
			}
		} else {
			return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null);
		}
	}
	
	//Helper function handles the read transfer function
	public GPUOptimizationLatticeElement read(String index) {
		boolean indexMatch = indexMatch(this.index,index);
		
		if(isInvalidType()) {
			return this.use();
		}
		
		if(this.state == GPUOptimizationStates.INVALID) {
			return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null);
		}
		
		if (this.state == GPUOptimizationStates.ASSIGNED) {
			//If the array has not been read from then set the array to read from the given index
			return new GPUOptimizationLatticeElement(GPUOptimizationStates.READ_INDEX,index);
		}
		
		if(indexMatch) {
			if ( (this.state == GPUOptimizationStates.READ_INDEX) ||
				 (this.state == GPUOptimizationStates.READ_MULTI) ) {
				//If indexes match and they are all reads keep the current state
				return this.copy();
			} else if ( (this.state == GPUOptimizationStates.WRITE_INDEX) ||
					 	(this.state == GPUOptimizationStates.READ_WRITE_INDEX) ) {
				//This could be only an else statement but is kept robust to make the transition clearer
				//If indexes match and the previous state had a write then go to read write state
				return new GPUOptimizationLatticeElement(GPUOptimizationStates.READ_WRITE_INDEX,index,score);
			}
		} else {
			if ( (this.state == GPUOptimizationStates.READ_INDEX) ||
				 (this.state == GPUOptimizationStates.READ_MULTI) ) {
				//If indexes do not match and all operations are reads set state to multi-read
				return new GPUOptimizationLatticeElement(GPUOptimizationStates.READ_MULTI,null,score);
			} else {
				//Indexes mismatch and the array had a previous write go to invalid
				//Cannot handle multiple index writes
				return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null);
			}
				
		}
		
		return null; //Actually dead code
	}
	
	//Helper function handles parameter scoring
	public GPUOptimizationLatticeElement score(long score) {
		if(isInvalidType()) {
			return this.copy();
		}
		
		return new GPUOptimizationLatticeElement(
				this.state,
				this.index,
				this.score + score);		
	}
	
	//Returns a copy of the element that has been changed to assigned
	//This is used in for loop branching
	public GPUOptimizationLatticeElement assign() {
		if(isInvalidType()) {
			return getInvalidType(); //Set to the UNUSED state
		}
		
		return new GPUOptimizationLatticeElement(GPUOptimizationStates.ASSIGNED,null);
	}
	
	public GPUOptimizationLatticeElement use(){
		return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID_TYPE_USED,null);
	}
	
	public boolean isBottom(){
		return this.state == GPUOptimizationStates.ASSIGNED;
	}

	public boolean isInvalidType(){
		return this.state == GPUOptimizationStates.INVALID_TYPE_USED ||
			   this.state == GPUOptimizationStates.INVALID_TYPE_UNUSED;
	}
	
	public boolean isTop(){
		return this.state == GPUOptimizationStates.INVALID ||
			   this.state == GPUOptimizationStates.INVALID_TYPE_USED;
	}
	
	public GPUOptimizationStates getState(){
		return state;
	}
	
	public String getIndex(){
		return index;
	}
	
	public long getScore(){
		return score;
	}
	
	//STATIC METHODS-------------------------------------------------------
	public static GPUOptimizationLatticeElement bottom(){
		return new GPUOptimizationLatticeElement(GPUOptimizationStates.ASSIGNED,null);
	}
	
	public static GPUOptimizationLatticeElement top(){
		return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID,null);
	}
	
	public static GPUOptimizationLatticeElement getInvalidType(){
		return new GPUOptimizationLatticeElement(GPUOptimizationStates.INVALID_TYPE_UNUSED,null);
	}
	
	public static long approvedMethods(String methodName) {
		Map<String,Long> scoring = new TreeMap<String,Long>();
		
		scoring.put("abs",1l);
		scoring.put("acos",5l);
		scoring.put("asin",5l);
		scoring.put("atan",5l);
		scoring.put("atan2",5l);
		scoring.put("cos",5l);
		scoring.put("sin",5l);
		scoring.put("floor",1l);
		scoring.put("ceil",1l);
		scoring.put("round",1l);
		scoring.put("max",1l);
		scoring.put("min",1l);
		scoring.put("exp",5l);
		scoring.put("log",5l);
		scoring.put("pow",5l);
		scoring.put("sqrt",5l);
		scoring.put("IEEEremainder",5l);
		scoring.put("rint",5l);
		
		if(scoring.containsKey(methodName))
			return scoring.get(methodName);
		
		return -1l;
	}
	
	private static boolean indexMatch(String index1, String index2) {
		if(index1 == null && index2 == null)
			return true;
		else if(index1 == null || index2 == null)
			return false;
		else
			return index1.equals(index2);
	}
	
}
