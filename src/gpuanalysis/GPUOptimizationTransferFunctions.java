/*
 * Author: Jonathan Mansur
 * 
 * This code is meant to analyze for loops to determine if they can be parallelized for
 * the Aparapi Framework.
 * 
 */
package gpuanalysis;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import edu.cmu.cs.crystal.flow.BooleanLabel;
import edu.cmu.cs.crystal.flow.ILabel;
import edu.cmu.cs.crystal.flow.ILatticeOperations;
import edu.cmu.cs.crystal.flow.IResult;
import edu.cmu.cs.crystal.flow.LabeledResult;
import edu.cmu.cs.crystal.simple.TupleLatticeElement;
import edu.cmu.cs.crystal.simple.TupleLatticeOperations;
import edu.cmu.cs.crystal.tac.AbstractTACBranchSensitiveTransferFunction;
import edu.cmu.cs.crystal.tac.model.ArrayInitInstruction;
import edu.cmu.cs.crystal.tac.model.BinaryOperation;
import edu.cmu.cs.crystal.tac.model.CopyInstruction;
import edu.cmu.cs.crystal.tac.model.DotClassInstruction;
import edu.cmu.cs.crystal.tac.model.LoadArrayInstruction;
import edu.cmu.cs.crystal.tac.model.LoadFieldInstruction;
import edu.cmu.cs.crystal.tac.model.LoadLiteralInstruction;
import edu.cmu.cs.crystal.tac.model.MethodCallInstruction;
import edu.cmu.cs.crystal.tac.model.NewArrayInstruction;
import edu.cmu.cs.crystal.tac.model.StoreArrayInstruction;
import edu.cmu.cs.crystal.tac.model.UnaryOperation;
import edu.cmu.cs.crystal.tac.model.Variable;

public class GPUOptimizationTransferFunctions extends
	AbstractTACBranchSensitiveTransferFunction<TupleLatticeElement<Variable,GPUOptimizationLatticeElement>> {

	TupleLatticeOperations<Variable, GPUOptimizationLatticeElement> ops =
			new TupleLatticeOperations<Variable, GPUOptimizationLatticeElement>(
					new GPUOptimizationOperations(), 
					GPUOptimizationLatticeElement.bottom());
	
	@Override
	public TupleLatticeElement<Variable, GPUOptimizationLatticeElement> createEntryValue(
			MethodDeclaration arg0) {
		
		TupleLatticeElement<Variable, GPUOptimizationLatticeElement> def = ops.getDefault();
		def.put(getAnalysisContext().getThisVariable(), GPUOptimizationLatticeElement.bottom()); //Default BOTTOM
		
		return def;
	}

	@Override
	public ILatticeOperations<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> getLatticeOperations() {
		return ops;
	}

	//ARRAY CREATION--------------------------------------------------------------
	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			ArrayInitInstruction instr,
			List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {
		
		List<Variable> inits = instr.getInitOperands();
		Variable var = inits.get(0);

		if(var.resolveType() != null) {
			if(var.resolveType().isPrimitive())
				value.put(
						instr.getTarget(), 
						GPUOptimizationLatticeElement.bottom());
			else
				value.put(
						instr.getTarget(), 
						GPUOptimizationLatticeElement.getInvalidType());
		}
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			NewArrayInstruction instr,
			List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		
		
		if (instr.getArrayType().getComponentType().isPrimitiveType() &&
				instr.getDimensions() == 1)
			value.put(
					instr.getTarget(), 
					GPUOptimizationLatticeElement.bottom());
		else
			value.put(
					instr.getTarget(),
					GPUOptimizationLatticeElement.getInvalidType());
		
		return LabeledResult.createResult(labels, value);
	}

	//ARRAY READ--------------------------------------------------------------
	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			LoadArrayInstruction instr,
			List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {
		
		//Get array index
		String index = instr.getArrayIndex().getSourceString();
		
		//Get source element
		GPUOptimizationLatticeElement src = value.get(instr.getSourceArray()); 
		
		//Copy the input with a read function applied to it
		value.put(instr.getSourceArray(), src.read(index));
		
		return LabeledResult.createResult(labels, value);
	}

	//ARRAY WRITE--------------------------------------------------------------
	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			StoreArrayInstruction instr,
			List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		//Get array index
		String index = instr.getArrayIndex().getSourceString();
		
		//Get source element
		GPUOptimizationLatticeElement src = value.get(instr.getDestinationArray()); 
		
		//Copy the input with a write function applied to it
		value.put(instr.getDestinationArray(), src.write(index));
		
		return LabeledResult.createResult(labels, value);
	}

	//ARRAY BRANCHING & SCORING (Helper)--------------------------------------------------------------
	private TupleLatticeElement<Variable, GPUOptimizationLatticeElement> helpAssign(
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> inValue) {
		
		TupleLatticeElement<Variable, GPUOptimizationLatticeElement> outValue = ops.getDefault();
		
		Iterator<Variable> it = inValue.getKeySet().iterator();
		Variable key;
		GPUOptimizationLatticeElement elem;
		
		while(it.hasNext()) {
			//Get input key-lattice pair
			key = it.next();
			elem = inValue.get(key);
			
			//Copy the assignment to a new tuple with the element set to assigned
			outValue.put(key, elem.assign());
		}
		return outValue;
	}
	
	private TupleLatticeElement<Variable, GPUOptimizationLatticeElement> helpScore(
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> inValue,
			long score) {
		
		TupleLatticeElement<Variable, GPUOptimizationLatticeElement> outValue = ops.getDefault();
		
		Iterator<Variable> it = inValue.getKeySet().iterator();
		Variable key;
		GPUOptimizationLatticeElement elem;
		
		while(it.hasNext()) {
			//Get input key-lattice pair
			key = it.next();
			elem = inValue.get(key);
			
			//Copy the assignment to a new tuple with the element set to assigned
			outValue.put(key, elem.score(score));
		}
		return outValue;
	}
	
	private TupleLatticeElement<Variable, GPUOptimizationLatticeElement> helpInvalid(
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> inValue) {
		
		TupleLatticeElement<Variable, GPUOptimizationLatticeElement> outValue = ops.getDefault();
		
		Iterator<Variable> it = inValue.getKeySet().iterator();
		Variable key;
		
		while(it.hasNext()) {
			//Get input key-lattice pair
			key = it.next();
			
			//Copy the assignment to a new tuple with the element set to invalid
			outValue.put(key, GPUOptimizationLatticeElement.top());
		}
		return outValue;
	}
	
	//ARRAY BRANCHING & SCORING--------------------------------------------------------------
	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			BinaryOperation binop, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {
		
		value = helpScore(value,1);
		
		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value); 
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			CopyInstruction instr, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		if(value.get(instr.getOperand()) != null)
			value.put(instr.getTarget(), value.get(instr.getOperand()));
		
		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value); 
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			DotClassInstruction instr, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {
		
		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value);
		else
			value = helpInvalid(value);
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			LoadFieldInstruction instr, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value);
		else
			value = helpInvalid(value);
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			LoadLiteralInstruction instr, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value); 
		
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			MethodCallInstruction instr, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value);
		else {
			long score = GPUOptimizationLatticeElement.approvedMethods(instr.getMethodName());
			
			if((score >=0) && instr.isStaticMethodCall())
				value = helpScore(value,score);
			else
				value = helpInvalid(value);
		}
		return LabeledResult.createResult(labels, value);
	}

	@Override
	public IResult<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> transfer(
			UnaryOperation unop, List<ILabel> labels,
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> value) {

		value = helpScore(value,1);
		
		//Reset element upon branching for "for" loops
		if (labels.contains(BooleanLabel.getBooleanLabel(true)) && labels.contains(BooleanLabel.getBooleanLabel(false)))
			value = helpAssign(value); 
		
		return LabeledResult.createResult(labels, value);
	}
	
	
}
