/*
 * Author: Jonathan Mansur
 * 
 * This code is meant to analyze for loops to determine if they can be parallelized for
 * the Aparapi Framework.
 * 
 */
package gpuanalysis;

import java.util.Iterator;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import edu.cmu.cs.crystal.AbstractCrystalMethodAnalysis;
import edu.cmu.cs.crystal.simple.TupleLatticeElement;
import edu.cmu.cs.crystal.tac.TACFlowAnalysis;
import edu.cmu.cs.crystal.tac.model.Variable;

public class GPUOptimizationAnalysis extends AbstractCrystalMethodAnalysis {


	TACFlowAnalysis<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>> flowAnalysis;
	
	@Override
	public void analyzeMethod(MethodDeclaration d) {
		GPUOptimizationTransferFunctions tf = new GPUOptimizationTransferFunctions();
		flowAnalysis = new TACFlowAnalysis<TupleLatticeElement<Variable, GPUOptimizationLatticeElement>>(tf, getInput().getComUnitTACs().unwrap());
		
		d.accept(new GPUOptimizationVisitor());
	}

	@Override
	public String getName(){
		return "GPU Optimization Candidates";
	}
	
	private class GPUOptimizationVisitor extends ASTVisitor {

		@SuppressWarnings("deprecation")
		@Override
		public void endVisit(ForStatement node) {
			//Examines a for loop after the analysis is completed
			TupleLatticeElement<Variable, GPUOptimizationLatticeElement> afterTuple = flowAnalysis.getResultsAfter(node);
			
			long score = checkFor(afterTuple);
			
			//Only print warnings for loops that can be optimized
			//The helper function returns -1 to indicate an invalid state
			if(score >= 0)
				getReporter().reportUserProblem("This loop can be optimized (" + score + ")", node, getName());
			
		}

		private long checkFor(TupleLatticeElement<Variable, GPUOptimizationLatticeElement> afterTuple) {
			
			/*
			 * The helper function iterates through all of the elements looking for an invalid
			 * element and keeps track of the score
			 */
			
			Iterator<Variable> it = afterTuple.getKeySet().iterator();
			GPUOptimizationLatticeElement elem;
			
			long score = 0;
			
			while(it.hasNext()) {
				elem = afterTuple.get(it.next());
				
				score = Math.max(score, elem.getScore());
				
				if(elem.isTop())
					return -1; //-1 indicates that this loop is not a good candidate
			}
			
			return score;
		}
	}
}
