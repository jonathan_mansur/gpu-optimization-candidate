/*
 * Author: Jonathan Mansur
 * 
 * This code is meant to analyze for loops to determine if they can be parallelized for
 * the Aparapi Framework.
 * 
 */
package gpuanalysis;

import edu.cmu.cs.crystal.simple.SimpleLatticeOperations;

public class GPUOptimizationOperations extends
		SimpleLatticeOperations<GPUOptimizationLatticeElement> {

	@Override
	public boolean atLeastAsPrecise(GPUOptimizationLatticeElement left,
			                        GPUOptimizationLatticeElement right) {
		
		if(left.getState() == right.getState()) {
			return true;
		} else if(left.isBottom()) {
			return true;
		} else if(right.isTop() || right.isInvalidType()) {
			return true;
		} else if (left.getState() == GPUOptimizationStates.READ_INDEX){
			if(right.getState() == GPUOptimizationStates.READ_MULTI) {
				return true;
			} else if(right.getState() == GPUOptimizationStates.READ_WRITE_INDEX) {
				return true;
			} else
				return false;
		} else if (left.getState() == GPUOptimizationStates.WRITE_INDEX){
			if(right.getState() == GPUOptimizationStates.READ_WRITE_INDEX) {
				return true;
			} else
				return false;
		}
		
		return false;
	}

	@Override
	public GPUOptimizationLatticeElement bottom() {
		return GPUOptimizationLatticeElement.bottom();
	}

	@Override
	public GPUOptimizationLatticeElement copy(GPUOptimizationLatticeElement elem) {
		return elem.copy();
	}

	@Override
	public GPUOptimizationLatticeElement join(
			GPUOptimizationLatticeElement left,
			GPUOptimizationLatticeElement right) {
		
		if(left.equals(right)) {
			return left.copy();
		} else if (left.getState() == GPUOptimizationStates.INVALID_TYPE_USED ||
				  right.getState() == GPUOptimizationStates.INVALID_TYPE_USED){
			return GPUOptimizationLatticeElement.getInvalidType().use();
		} else if (left.getState() == GPUOptimizationStates.INVALID_TYPE_UNUSED ||
				  right.getState() == GPUOptimizationStates.INVALID_TYPE_UNUSED){
			return GPUOptimizationLatticeElement.getInvalidType();
		} else if(left.isBottom()) {
			return right.copy();
		} else if(right.isBottom()) {
			return left.copy();
		} else if((left.isTop()) || (right.isTop())) {
			return GPUOptimizationLatticeElement.top(); 
		} else if (left.getState() == GPUOptimizationStates.READ_INDEX){
			return right.read(left.getIndex());
			
		} else if (left.getState() == GPUOptimizationStates.WRITE_INDEX){
			return right.write(left.getIndex());
			
		} else if (left.getState() == GPUOptimizationStates.READ_MULTI){
			if(right.getState() == GPUOptimizationStates.READ_MULTI)
				return left.copy();
			else
				return GPUOptimizationLatticeElement.top();
		} else if (left.getState() == GPUOptimizationStates.READ_WRITE_INDEX){
			if (right.getState() == GPUOptimizationStates.READ_INDEX){
				return left.read(right.getIndex());
			} else if (right.getState() == GPUOptimizationStates.WRITE_INDEX){
				return left.write(right.getIndex());
			}
		}
		
		return GPUOptimizationLatticeElement.top();
	}

}
