/*
 * Author: Jonathan Mansur
 * 
 * This code is meant to analyze for loops to determine if they can be parallelized for
 * the Aparapi Framework.
 * 
 */
package gpuanalysis;

public enum GPUOptimizationStates {
	ASSIGNED, READ_MULTI, READ_INDEX, WRITE_INDEX, READ_WRITE_INDEX, INVALID, INVALID_TYPE_UNUSED, INVALID_TYPE_USED
}
